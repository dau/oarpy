.. oarpy documentation master file, created by
   sphinx-quickstart on Mon Nov 19 09:33:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to oarpy's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :hidden:

   modules
   tutorials

:doc:`tutorials`
    Some examples on how to use the library
    
:doc:`modules`
    API documentation
    
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
