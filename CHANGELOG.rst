0.1.0:
------

 * Job: manage existing jobs (status, stop, suspend, resume)
 * JobFactory: define and launch jobs (creates Job)
 * Resource: optional OAR resources for JobFactory (nodes, cores, gpu)
 * Quick API: submit and search
